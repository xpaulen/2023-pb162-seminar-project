package cz.muni.fi.pb162.project;

import java.util.Comparator;

/**
 * Coordinates comparator for reversed comparison of coordinates
 *
 * @author Adam Paulen
 */
public class CoordinatesComparator implements Comparator<Coordinates> {
    @Override
    public int compare(Coordinates o1, Coordinates o2) {
        return -1 * o1.compareTo(o2);
    }
}
