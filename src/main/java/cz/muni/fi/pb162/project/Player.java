package cz.muni.fi.pb162.project;

/**
 * Player record representing a player
 *
 * @param name name of the player
 * @param color color of the player
 * @author Adam Paulen
 */
public record Player(String name, Color color) {

    @Override
    public String toString() {
        return name + "-" + color.toString();
    }
}
