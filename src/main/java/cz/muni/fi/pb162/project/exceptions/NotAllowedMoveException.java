package cz.muni.fi.pb162.project.exceptions;

/**
 * exception when player tries to do illegal move.
 *
 * @author Adam Paulen
 */
public class NotAllowedMoveException extends Exception {
    /**
     * empty Constructor
     */
    public NotAllowedMoveException() {
        super();
    }

    /**
     * constructor with error message.
     *
     * @param message error message.
     */
    public NotAllowedMoveException(String message) {
        super(message);
    }

    /**
     * Constructor with error message and cause of the error.
     *
     * @param message error message
     * @param cause cause
     */
    public NotAllowedMoveException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor with cause.
     *
     * @param cause cause
     */
    public NotAllowedMoveException(Throwable cause) {
        super(cause);
    }
}
