package cz.muni.fi.pb162.project.exceptions;

/**
 * exception when player does not provide correct input.
 *
 * @author Adam Paulen
 */
public class InvalidFormatOfInputException extends RuntimeException {
    /**
     * empty Constructor
     */
    public InvalidFormatOfInputException() {
        super();
    }

    /**
     * constructor with error message.
     *
     * @param message error message.
     */
    public InvalidFormatOfInputException(String message) {
        super(message);
    }

    /**
     * Constructor with error message and cause of the error.
     *
     * @param message error message
     * @param cause cause
     */
    public InvalidFormatOfInputException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor with cause.
     *
     * @param cause cause
     */
    public InvalidFormatOfInputException(Throwable cause) {
        super(cause);
    }
}
