package cz.muni.fi.pb162.project.exceptions;

/**
 * exception when a player wants to move from empty square or out of bounds.
 *
 * @author Adam Paulen
 */
public class EmptySquareException extends Exception {

    /**
     * empty Constructor
     */
    public EmptySquareException() {
        super();
    }

    /**
     * constructor with error message.
     *
     * @param message error message.
     */
    public EmptySquareException(String message) {
        super(message);
    }

    /**
     * Constructor with error message and cause of the error.
     *
     * @param message error message
     * @param cause cause
     */
    public EmptySquareException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor with cause.
     *
     * @param cause cause
     */
    public EmptySquareException(Throwable cause) {
        super(cause);
    }
}
