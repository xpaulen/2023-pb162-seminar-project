package cz.muni.fi.pb162.project;

import cz.muni.fi.pb162.project.exceptions.EmptySquareException;
import cz.muni.fi.pb162.project.exceptions.InvalidFormatOfInputException;
import cz.muni.fi.pb162.project.exceptions.NotAllowedMoveException;
import cz.muni.fi.pb162.project.utils.BoardNotation;


import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;
import java.util.Scanner;
import java.util.Comparator;
import java.util.Set;
import java.util.Arrays;
import java.util.TreeSet;
import java.util.Collection;

import java.util.stream.Collectors;


/**
 * class representing Game
 *
 * @author Adam Paulen
 */
public abstract class Game implements Playable {
    private final Player playerOne;
    private final Player playerTwo;
    private final Board board;
    private StateOfGame stateOfGame = StateOfGame.PLAYING;
    private static final Scanner SCANNER = new Scanner(System.in);
    private final Deque<Memento> mementoHistory = new ArrayDeque<>();


    /**
     * Game constructor taking in both players
     *
     * @param playerOne white player
     * @param playerTwo black player
     */
    public Game(Player playerOne, Player playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        board = new Board();
    }

    protected Game(Player playerOne, Player playerTwo, Board board) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.board = board;
    }

    public Player getPlayerOne() {
        return playerOne;
    }

    public Player getPlayerTwo() {
        return playerTwo;
    }

    public Board getBoard() {
        return board;
    }

    public StateOfGame getStateOfGame() {
        return stateOfGame;
    }

    public Deque<Memento> getMementoHistory() {
        return mementoHistory;
    }

    public void setStateOfGame(StateOfGame stateOfGame) {
        this.stateOfGame = stateOfGame;
    }

    /**
     * gets a player that is on turn
     *
     * @return a player on his turn
     */
    public Player getCurrentPlayer() {
        Player white = playerOne;
        Player black = playerTwo;
        if (playerOne.color().equals(Color.BLACK)) {
            black = playerOne;
            white = playerTwo;
        }
        if (board.getRound() % 2 == 0) {
            return white;
        }
        return black;
    }

    /**
     * puts a piece on a board
     *
     * @param letterNumber a letter coordinate on board
     * @param number       a number coordinate on the board
     * @param piece        a piece to be put on the board
     */
    public void putPieceOnBoard(int letterNumber, int number, Piece piece) {
        board.putPieceOnBoard(letterNumber, number, piece);
    }

    /**
     * Updates game status
     */
    public abstract void updateStatus();

    /**
     * Validates if the move can be done.
     *
     * @param coordsOne   Coordinates to move piece from
     * @param coordsTwo   Coordinates to move piece to
     * @param playerColor Color of the player
     * @return returns whether the move is possible
     */
    private boolean validMove(Coordinates coordsOne, Coordinates coordsTwo, Color playerColor){
        return Board.inRange(coordsOne) && Board.inRange(coordsTwo) && board.getPiece(coordsOne) != null;
    }

    /**
     * gets coordinates input from Player
     *
     * @return returns Coordinates from player input
     */
    private Coordinates getInputFromPlayer() {
        var position = SCANNER.next().trim();
        var letterNumber = position.charAt(0);
        int number;
        try {
            number = Integer.parseInt(String.valueOf(position.charAt(1)));
        } catch (NumberFormatException e) {
            throw new InvalidFormatOfInputException("integer expected char provided");
        }
        if (!Character.isLetter(letterNumber)) {
            throw new InvalidFormatOfInputException("letter expected");
        }
        return BoardNotation.getCoordinatesOfNotation(letterNumber, number);
    }

    /**
     * Makes a move of piece from given Coordinates to given Coordinates
     *
     * @param coordinatesOne move from
     * @param coordinatesTwo move to
     */
    public void move(Coordinates coordinatesOne, Coordinates coordinatesTwo) {
        Piece piece = board.getPiece(coordinatesOne);
        if (!validMove(coordinatesOne, coordinatesTwo, piece.getColor())) {
            return;
        }
        putPieceOnBoard(coordinatesOne.letterNumber(), coordinatesOne.number(), null);
        putPieceOnBoard(coordinatesTwo.letterNumber(), coordinatesTwo.number(), piece);
    }

    @Override
    public boolean equals(Object otherGame) {
        if (this == otherGame) {
            return true;
        }
        if (!(otherGame instanceof Game)) {
            return false;
        }
        Game game = (Game) otherGame;
        return Objects.equals(playerOne, game.playerOne)
                && Objects.equals(playerTwo, game.playerTwo)
                && Objects.equals(board, game.board)
                && stateOfGame == game.stateOfGame;
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerOne, playerTwo, board, stateOfGame);
    }

    @Override
    public void play() throws EmptySquareException, NotAllowedMoveException {
        setInitialSet();
        while (stateOfGame.equals(StateOfGame.PLAYING)) {
            Player player = getCurrentPlayer();
            Coordinates coordsOne = getInputFromPlayer();
            if (board.isEmpty(coordsOne.letterNumber(), coordsOne.number())){
                throw new EmptySquareException("empty space or out of bounds");
            }
            Coordinates coordsTwo = getInputFromPlayer();
            if (!allPossibleMovesByCurrentPlayer().contains(coordsTwo)){
                throw new NotAllowedMoveException("illegal move");
            }
            move(coordsOne, coordsTwo);
            board.setRound(board.getRound() + 1);
            updateStatus();
            hitSave();
        }
    }

    @Override
    public void hitSave() {
        mementoHistory.push(board.save());
    }

    @Override
    public void hitUndo() {
        if (mementoHistory.isEmpty()){
            return;
        }
        Memento save = mementoHistory.pop();
        board.restore(save);
    }

    /**
     * gets all possible moves by the player on turn
     *
     * @return sorted set of possible moves
     */
    public Set<Coordinates> allPossibleMovesByCurrentPlayer(){
        Comparator<Coordinates> comparator = new Comparator<Coordinates>() {
            @Override
            public int compare(Coordinates o1, Coordinates o2) {
                return -1 * o1.compareTo(o2);
            }
        };
        Player player = getCurrentPlayer();
        Piece[] pieces = board.getAllPiecesFromBoard();
        pieces = Arrays.stream(pieces)
                .filter(p -> p.getColor() == player.color())
                .toArray(Piece[]::new);
        return Arrays.stream(pieces)
                .map(p -> p.getAllPossibleMoves(this))
                .flatMap(Collection::stream)
                .collect(Collectors.toCollection(() -> new TreeSet<>(comparator.reversed())));
    }
}
