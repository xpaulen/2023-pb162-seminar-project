package cz.muni.fi.pb162.project;

import cz.muni.fi.pb162.project.exceptions.EmptySquareException;
import cz.muni.fi.pb162.project.exceptions.NotAllowedMoveException;

/**
 * interface for a Playable game.
 *
 * @author Adam Paulen
 */
public interface Playable extends Caretaker {

    /**
     * Sets the board to the initial state.
     *
     */
    void setInitialSet();

    /**
     * moves a piece from the first coordinate to the other one.
     * if pawn touches the end of the board promotion to queen happens.
     * if there is no piece at the first coordinate
     * or the move is illegal nothing happens.
     *
     * @param coordinatesOne coordinate to move piece from
     * @param coordinatesTwo coordinate to move piece to
     */
    void move(Coordinates coordinatesOne, Coordinates coordinatesTwo);

    /**
     * play is the method to simulate a game. The method loops
     * until the game is over. each loop it takes an input from
     * the player ta make a move. The method checks if the game ended.
     *
     */
    void play() throws EmptySquareException, NotAllowedMoveException;

}
