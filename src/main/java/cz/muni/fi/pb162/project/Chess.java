package cz.muni.fi.pb162.project;

import cz.muni.fi.pb162.project.exceptions.MissingPlayerException;
import cz.muni.fi.pb162.project.utils.BoardNotation;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * class for Game chess
 *
 * @author Adam Paulen
 */
public class Chess extends Game implements GameWritable {

    private static final PieceType[] PIECE_ORDER = {PieceType.ROOK, PieceType.KNIGHT,
            PieceType.BISHOP, PieceType.QUEEN, PieceType.KING,
            PieceType.BISHOP, PieceType.KNIGHT, PieceType.ROOK};

    /**
     * Game constructor taking in both players
     *
     * @param playerOne white player
     * @param playerTwo black player
     */
    public Chess(Player playerOne, Player playerTwo) {
        super(playerOne, playerTwo);
    }

    private Chess(Player playerOne, Player playerTwo, Board board) {
        super(playerOne, playerTwo, board);

    }

    /*
    @Override
    public void updateStatus() {
        Board board = getBoard();
        Piece[] pieces = board.getAllPiecesFromBoard();
        Color winner = null;
        int counter = 0;
        for (Piece piece : pieces) {
            if (piece.getPieceType().equals(PieceType.KING)) {
                winner = piece.getColor();
                counter++;
            }
        }
        if (counter == 1) {
            StateOfGame state = winner == Color.WHITE ? StateOfGame.WHITE_PLAYER_WIN : StateOfGame.BLACK_PLAYER_WIN;
            setStateOfGame(state);
        }
    }
     */

    @Override
    public void updateStatus() {
        Board board = getBoard();
        Piece[] pieces = board.getAllPiecesFromBoard();
        List<Piece> winner = Arrays.stream(pieces).filter(p -> p.getPieceType().equals(PieceType.KING)).toList();
        if (winner.size() == 1) {
            StateOfGame state = winner.iterator().next().getColor() == Color.WHITE ?
                    StateOfGame.WHITE_PLAYER_WIN :
                    StateOfGame.BLACK_PLAYER_WIN;
            setStateOfGame(state);
        }
    }

    @Override
    public void setInitialSet() {
        for (Color color : new Color[]{Color.WHITE, Color.BLACK}) {
            int col = 0;
            int sign = color.equals(Color.WHITE) ? 1 : -1;
            int row = color.equals(Color.WHITE) ? 0 : 7;
            for (PieceType type : PIECE_ORDER) {
                putPieceOnBoard(col, row, new Piece(color, type));
                putPieceOnBoard(col, row + sign, new Piece(color, PieceType.PAWN));
                col++;
            }
        }
    }

    /**
     * Promotes a piece to queen if it was a pawn
     * and is on the end of the board
     *
     * @param coords coordinates of the piece
     * @param piece  piece to be potentially promoted
     */
    private void promotion(Coordinates coords, Piece piece) {
        if (piece.getPieceType().equals(PieceType.PAWN) && (coords.number() == 0 || coords.number() == 7)) {
            putPieceOnBoard(coords.letterNumber(), coords.number(), new Piece(piece.getColor(), PieceType.QUEEN));
        }
    }

    @Override
    public void move(Coordinates coordinatesOne, Coordinates coordinatesTwo) {
        Board board = getBoard();
        Piece piece = board.getPiece(coordinatesOne);
        super.move(coordinatesOne, coordinatesTwo);
        if (piece != null) {
            promotion(coordinatesTwo, piece);
        }
    }

    @Override
    public void write(OutputStream os) throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8));

        writer.write(String.format("%s-%s;%s-%s", getPlayerOne().name(), getPlayerOne().color(),
                getPlayerTwo().name(), getPlayerTwo().color()));

        writer.newLine();

        for (int row = 0; row < Board.SIZE; row++) {
            for (int col = 0; col < Board.SIZE; col++) {
                Piece piece = getBoard().getPiece(row, col);
                if (piece == null) {
                    writer.write("_");
                } else {
                    writer.write(String.format("%s,%s", piece.getPieceType(), piece.getColor()));
                }
                if (col != Board.SIZE - 1){
                    writer.write(";");
                }
            }
            writer.newLine();
        }

        writer.flush();
    }

    @Override
    public void write(File file) throws IOException {
        try(FileOutputStream fos = new FileOutputStream(file)){
            write(fos);
        }
    }

    /**
     * Builder for Chess game.
     *
     * @author Adam Paulen
     */

    public static class Builder implements Buildable<Chess>, GameReadable {
        private final List<Player> players = new ArrayList<>();
        private final Board board = new Board();


        /**
         * Adds player to chess game.
         *
         * @param player player to be added
         * @return Builder
         */
        public Builder addPlayer(Player player) {
            if (players.size() < 2 && !players.contains(player)) {
                players.add(player);
            }
            return this;
        }

        /**
         * adds piece to Chess game.
         *
         * @param piece  piece to be added.
         * @param letter position where to add
         * @param number position where to add
         * @return Builder
         */
        public Builder addPieceToBoard(Piece piece, char letter, int number) {
            Coordinates coords = BoardNotation.getCoordinatesOfNotation(letter, number);
            board.putPieceOnBoard(coords, piece);
            return this;
        }

        /**
         * Builds the Chess game.
         *
         * @return Chess game
         */
        public Chess build() {
            if (players.size() < 2) {
                throw new MissingPlayerException("2 players needs to be provided");
            }

            return new Chess(players.get(0), players.get(1), board);
        }

        @Override
        public Builder read(InputStream is) throws IOException {
            return read(is, false);
        }

        private void readHeader(String header) throws IOException{
            String[] playersHeader = header.split(";");
            if (playersHeader.length != 2) {
                throw new IOException("Invalid number of players");
            }
            players.add(new Player(playersHeader[0].split("-")[0], Color.WHITE));
            players.add(new Player(playersHeader[0].split("-")[0], Color.BLACK));
        }

        private void createAndAddPiece(String[] rowData, int col) throws IOException{
            for (int row = 0; row < Board.SIZE; row++) {
                String pieceInfo = rowData[row];
                if (pieceInfo.equals("_")) {
                    continue;
                }
                String[] info = pieceInfo.split(",");
                String pieceType = info[0].toUpperCase();
                Color color = info[1].equals("WHITE") ? Color.WHITE : Color.BLACK;
                Piece piece;
                switch (pieceType) {
                    case "PAWN" -> piece = new Piece(color, PieceType.PAWN);
                    case "ROOK" -> piece = new Piece(color, PieceType.ROOK);
                    case "KNIGHT" -> piece = new Piece(color, PieceType.KNIGHT);
                    case "BISHOP" -> piece = new Piece(color, PieceType.BISHOP);
                    case "QUEEN" -> piece = new Piece(color, PieceType.QUEEN);
                    case "KING" -> piece = new Piece(color, PieceType.KING);
                    default -> throw new IOException("Invalid piece type");
                }
                board.putPieceOnBoard(row, col, piece);
            }
        }

        @Override
        public Builder read(InputStream is, boolean hasHeader) throws IOException {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            if (hasHeader) {
                readHeader(reader.readLine());
            }
            List<String[]> pieceData = new ArrayList<>();
            String line;
            while ((line = reader.readLine()) != null) {
                pieceData.add(line.split(";"));
            }

            reader.close();
            if (pieceData.size() != Board.SIZE) {
                throw new IOException("Invalid number of rows in board layout");
            }
            for (int col = 0; col < Board.SIZE; col++) {
                String[] rowData = pieceData.get(col);
                if (rowData.length != Board.SIZE) {
                    throw new IOException("Invalid number of pieces in row");
                }
                createAndAddPiece(rowData, col);
            }
            return this;
        }

        @Override
        public Builder read(File file) throws IOException {
            return read(file, false);
        }

        @Override
        public Builder read(File file, boolean hasHeader) throws IOException {
            try(FileInputStream fis = new FileInputStream(file)){
                return read(fis, hasHeader);
            }
        }
    }
}
