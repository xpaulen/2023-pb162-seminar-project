package cz.muni.fi.pb162.project;

/**
 * class for Game draughts
 *
 * @author Adam Paulen
 */
public class Draughts extends Game {

    /**
     * Game constructor taking in both players
     *
     * @param playerOne white player
     * @param playerTwo black player
     */
    public Draughts(Player playerOne, Player playerTwo) {
        super(playerOne, playerTwo);
    }

    @Override
    public void updateStatus() {
        Board board = getBoard();
        Piece[] pieces = board.getAllPiecesFromBoard();
        Color lastColor = pieces[0].getColor();
        for (Piece piece : pieces) {
            if(!lastColor.equals(piece.getColor())){
                return;
            }
        }
        if (lastColor.equals(Color.WHITE)) {
            setStateOfGame(StateOfGame.WHITE_PLAYER_WIN);
            return;
        }
        setStateOfGame(StateOfGame.BLACK_PLAYER_WIN);
    }

    @Override
    public void setInitialSet() {
        for (Color color : new Color[]{Color.WHITE, Color.BLACK}) {
            for (int col = 0; col < Board.SIZE; col += 2) {
                for (int row = 0; row < 3; row++) {
                    int shiftCol = row % 2 == 0 ? 0 : 1;
                    if (color.equals(Color.BLACK)) {
                        shiftCol = (shiftCol + 1) % 2;
                    }
                    int shiftRow = color.equals(Color.BLACK) ? 5 : 0;
                    putPieceOnBoard(col + shiftCol, row + shiftRow, new Piece(color, PieceType.DRAUGHTS_MAN));
                }
            }
        }
    }

    /**
     * Promotes a piece to king if it was a man
     * and is on the end of the board
     *
     * @param coords coordinates of the piece
     * @param piece piece to be potentially promoted
     */
    private void promotion(Coordinates coords, Piece piece) {
        if (piece.getPieceType().equals(PieceType.DRAUGHTS_MAN) &&
                ((coords.number() == 0 && piece.getColor().equals(Color.BLACK))
                        || (coords.number() == 7) && piece.getColor().equals(Color.WHITE))) {
            putPieceOnBoard(coords.letterNumber(), coords.number(),
                    new Piece(piece.getColor(), PieceType.DRAUGHTS_KING));
        }
    }

    @Override
    public void move(Coordinates coordinatesOne, Coordinates coordinatesTwo) {
        Board board = getBoard();
        Piece piece = board.getPiece(coordinatesOne);
        super.move(coordinatesOne, coordinatesTwo);
        if (piece != null) {
            promotion(coordinatesTwo, piece);
        }
    }
}
