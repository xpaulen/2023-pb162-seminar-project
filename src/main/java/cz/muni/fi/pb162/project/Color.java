package cz.muni.fi.pb162.project;

/**
 * enumeration type for Player Colors
 *
 * @author Adam Paulen
 */
public enum Color {
    WHITE,
    BLACK;

    /**
     * gets opposite color of opponent
     *
     * @return returns a color of opposite opponent
     */
    public Color getOppositeColor() {
        return Color.values()[(this.ordinal() + 1) % 2];
    }
}