package cz.muni.fi.pb162.project.exceptions;

/**
 * exception when game is being build with missing player.
 *
 * @author Adam Paulen
 */
public class MissingPlayerException extends RuntimeException {
    /**
     * empty Constructor
     */
    public MissingPlayerException() {
        super();
    }

    /**
     * constructor with error message.
     *
     * @param message error message.
     */
    public MissingPlayerException(String message) {
        super(message);
    }

    /**
     * Constructor with error message and cause of the error.
     *
     * @param message error message
     * @param cause cause
     */
    public MissingPlayerException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor with cause.
     *
     * @param cause cause
     */
    public MissingPlayerException(Throwable cause) {
        super(cause);
    }
}
