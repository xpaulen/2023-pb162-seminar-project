package cz.muni.fi.pb162.project;

import cz.muni.fi.pb162.project.utils.BoardNotation;


/**
 * record for coordinates of game board
 *
 * @param letterNumber row of the board
 * @param number column of the board
 * @author Adam Paulen
 */
public record Coordinates(int letterNumber, int number) implements Comparable<Coordinates> {

    /**
     * calculates average of coordinates
     *
     * @return average of letterNumber and number
     */
    public double averageOfCoordinates() {
        return (letterNumber + number) / 2.0;
    }

    /**
     * adds two coordinates and returns new coordinate with added values
     *
     * @param coords coordinates to be added to coordinates
     * @return new Coordinates with added values
     */
    public Coordinates add(Coordinates coords) {
        return new Coordinates(letterNumber + coords.letterNumber(), number + coords.number());
    }

    /**
     *  translates coordinates to string
     *
     * @return string of the coordinates
     */
    @Override
    public String toString() {
        return BoardNotation.getNotationOfCoordinates(letterNumber, number);
    }

    @Override
    public int compareTo(Coordinates o) {
        int result = this.letterNumber()- o.letterNumber();
        return result == 0 ? this.number() - o.number() : result;
    }
}
