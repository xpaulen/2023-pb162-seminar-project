package cz.muni.fi.pb162.project;

/**
 * stored game state
 *
 * @param board saved board state
 * @param round round that board was saved on
 * @author Adam Paulen
 */
public record Memento(Piece[][] board, int round) {

}
