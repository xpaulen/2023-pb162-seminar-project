package cz.muni.fi.pb162.project;

import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.Map;

/**
 * types of chess pieces and draughts pieces
 *
 * @author Adam Paulen
 */
public enum PieceType {
    PAWN,
    KNIGHT,
    BISHOP,
    ROOK,
    QUEEN,
    KING,
    DRAUGHTS_KING,
    DRAUGHTS_MAN;

    public static final Map<Pair<PieceType, Color>, String> UNI_MAP = new HashMap<>() {{
        put(Pair.of(PieceType.KING, Color.WHITE), "\u2654");
        put(Pair.of(PieceType.QUEEN, Color.WHITE), "\u2655");
        put(Pair.of(PieceType.QUEEN, Color.WHITE), "\u2655");
        put(Pair.of(PieceType.BISHOP, Color.WHITE), "\u2657");
        put(Pair.of(PieceType.ROOK, Color.WHITE), "\u2656");
        put(Pair.of(PieceType.KNIGHT, Color.WHITE), "\u2658");
        put(Pair.of(PieceType.PAWN, Color.WHITE), "\u2659");
        put(Pair.of(PieceType.KING, Color.BLACK), "\u265A");
        put(Pair.of(PieceType.QUEEN, Color.BLACK), "\u265B");
        put(Pair.of(PieceType.BISHOP, Color.BLACK), "\u265D");
        put(Pair.of(PieceType.ROOK, Color.BLACK), "\u265C");
        put(Pair.of(PieceType.KNIGHT, Color.BLACK), "\u265E");
        put(Pair.of(PieceType.PAWN, Color.BLACK), "\u265F");
        put(Pair.of(PieceType.DRAUGHTS_MAN, Color.WHITE), "\u26C0");
        put(Pair.of(PieceType.DRAUGHTS_KING, Color.WHITE), "\u26C1");
        put(Pair.of(PieceType.DRAUGHTS_MAN, Color.BLACK), "\u26C2");
        put(Pair.of(PieceType.DRAUGHTS_KING, Color.BLACK), "\u26C3");
    }};

    /**
     * gets symbol of pieceType and its color
     *
     * @param color specified color of the Piece
     * @return returns a symbol of a Piece
     */
    public String getSymbol(Color color){
        return UNI_MAP.getOrDefault(Pair.of(this, color), " ");
    }
}
