package cz.muni.fi.pb162.project.utils;

import cz.muni.fi.pb162.project.Coordinates;

/**
 * utility class for notating move on game board
 *
 * @author Adam Paulen
 */
public class BoardNotation {
    private static final char LETTER_A = 'a';

    /**
     * outputs notation of coordinates
     *
     * @param letterNumber row of board
     * @param number col of board
     * @return returns Notation of Coordinates in style "a5"
     */
    public static String getNotationOfCoordinates(int letterNumber, int number) {
        char letter = (char) (LETTER_A + letterNumber);
        return letter + Integer.toString(number + 1);
    }

    /**
     * gives coordinates of notation
     *
     * @param letter row on board
     * @param number col on board
     * @return returns coordinates in style
     */
    public static Coordinates getCoordinatesOfNotation(char letter, int number) {
        int letterNumber = letter - LETTER_A;
        return new Coordinates(letterNumber, number - 1);
    }
}