package cz.muni.fi.pb162.project;


import cz.muni.fi.pb162.project.moves.Diagonal;
import cz.muni.fi.pb162.project.moves.Knight;
import cz.muni.fi.pb162.project.moves.Straight;
import cz.muni.fi.pb162.project.moves.Move;
import cz.muni.fi.pb162.project.moves.Jump;
import cz.muni.fi.pb162.project.moves.Pawn;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * Piece class representing a game piece
 *
 * @author Adam Paulen
 */
public class Piece implements Prototype {
    private static final AtomicLong ID_COUNTER = new AtomicLong();
    private final long id;
    private final PieceType pieceType;
    private final Color color;
    private final List<Move> moveStrategies;

    /**
     * creates new piece object
     *
     */
    public Piece(Color color, PieceType pieceType) {
        id = ID_COUNTER.getAndIncrement();
        this.pieceType = pieceType;
        this.color = color;
        switch (pieceType) {
            case KING -> moveStrategies = List.of(new Straight(1), new Diagonal(1));
            case QUEEN -> moveStrategies = List.of(new Straight(), new Diagonal());
            case BISHOP -> moveStrategies = List.of(new Diagonal());
            case ROOK -> moveStrategies = List.of(new Straight());
            case KNIGHT -> moveStrategies = List.of(new Knight());
            case PAWN -> moveStrategies = List.of(new Pawn());
            case DRAUGHTS_KING -> moveStrategies = List.of(new Diagonal(1), new Jump());
            case DRAUGHTS_MAN -> moveStrategies = List.of(new Diagonal(1, true), new Jump(true));
            default -> throw new IllegalArgumentException("Unknown type in chess.");
        }
    }

    public long getId() {
        return id;
    }

    public Color getColor() {
        return color;
    }

    public PieceType getPieceType() {
        return pieceType;
    }

    @Override
    public Piece makeClone() {
        return new Piece(this.color, this.pieceType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return pieceType.getSymbol(color);
    }

    @Override
    public boolean equals(Object otherPiece) {
        if (this == otherPiece) {
            return true;
        }
        if (otherPiece == null || getClass() != otherPiece.getClass()) {
            return false;
        }
        Piece piece = (Piece) otherPiece;
        return id == piece.id;
    }

    /**
     * gets copy of moveStrategies
     *
     * @return copy of moveStrategies
     */
    public List<Move> getMoves() {
        return List.copyOf(moveStrategies);
    }

    /**
     * gets all moves possible with this piece
     *
     * @return set of all possible moves;
     */
    public Set<Coordinates> getAllPossibleMoves(Game game){
        Coordinates coords = game.getBoard().findCoordinatesOfPieceById(id);
        return moveStrategies.stream().map(c -> c.getAllowedMoves(game, coords))
                .flatMap(Collection::stream).collect(Collectors.toSet());
    }

    /*
    public Set<Coordinates> getAllPossibleMoves(Game game){

        Set<Coordinates> result = new HashSet<>();
        Coordinates coords = game.getBoard().findCoordinatesOfPieceById(id);
        for (Move move : moveStrategies){
            result.addAll(move.getAllowedMoves(game, coords));
        }
        return result;
    }

     */
}
