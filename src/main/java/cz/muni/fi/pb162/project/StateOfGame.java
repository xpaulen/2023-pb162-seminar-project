package cz.muni.fi.pb162.project;

/**
 * enumeration type of possible game states.
 *
 * @author Adam Paulen
 */
public enum StateOfGame {
    WHITE_PLAYER_WIN, BLACK_PLAYER_WIN, PAT, PLAYING;

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}
