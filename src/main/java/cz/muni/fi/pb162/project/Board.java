package cz.muni.fi.pb162.project;


import java.util.Arrays;
import java.util.Objects;

/**
 * Board class representing a game board
 *
 * @author Adam Paulen
 */
public class Board implements Originator<Memento> {
    public static final int SIZE = 8;
    private final Piece[][] board = new Piece[SIZE][SIZE];
    private int round = 0;

    private static final char[] LETTER_NUMBER_NOTATION = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    /**
     * inRange method checking if row and col are within the board
     *
     * @param row row of the board
     * @param col column of the board
     * @return true if row and col are in the range of board otherwise false
     */
    public static boolean inRange(int row, int col) {
        return (row < SIZE && row >= 0) && (col < SIZE && col >= 0);
    }

    /**
     * overloaded inRange method taking in class Coordinates
     *
     * @param coords Coordinates of the board
     * @return true if row and col are in the range of board otherwise false
     */
    public static boolean inRange(Coordinates coords) {
        return inRange(coords.letterNumber(), coords.number());
    }

    /**
     * isEmpty method checking the state of tile
     *
     * @param row row of the board
     * @param col column of the board
     * @return true if it is empty else false
     */
    public boolean isEmpty(int row, int col) {
        if (!inRange(row, col)) {
            return true;
        }
        return board[row][col] == null;
    }

    /**
     * getPiece method gets the piece on the tile
     *
     * @param row row of the board
     * @param col col of the board
     * @return piece on the tile
     */
    public Piece getPiece(int row, int col) {
        if (!inRange(row, col)) {
            return null;
        }
        return board[row][col];
    }

    /**
     * getPiece method gets the piece on the tile
     *
     * @param coords coords of the Piece
     * @return piece on the tile
     */
    public Piece getPiece(Coordinates coords) {
        return getPiece(coords.letterNumber(), coords.number());
    }


    /**
     * places a piece on the tile
     *
     * @param row   row of the board
     * @param col   col of the board
     * @param piece piece to be placed
     */
    public void putPieceOnBoard(int row, int col, Piece piece) {
        board[row][col] = piece;
    }

    /**
     * places a piece on the tile
     *
     * @param coordinates coords where to place the piece
     * @param piece       piece to be placed
     */
    public void putPieceOnBoard(Coordinates coordinates, Piece piece) {
        putPieceOnBoard(coordinates.letterNumber(), coordinates.number(), piece);
    }

    /**
     * finds a piece on the board by given id
     *
     * @param id id of the piece
     * @return return Coordinates of the piece if it was not found returns null
     */
    public Coordinates findCoordinatesOfPieceById(long id) {
        for (int row = 0; row < SIZE; row++) {
            for (int col = 0; col < SIZE; col++) {
                if (board[row][col] != null && board[row][col].getId() == id) {
                    return new Coordinates(row, col);
                }
            }
        }
        return null;
    }

    /**
     * gets all pieces that are currently on the board
     *
     * @return returns one-dimensional array of pieces
     */
    /*
    public Piece[] getAllPiecesFromBoard() {
        ArrayList<Piece> pieces = new ArrayList<>();
        for (int row = 0; row < SIZE; row++) {
            for (int col = 0; col < SIZE; col++) {
                if (board[row][col] != null) {
                    pieces.add(board[row][col]);
                }
            }
        }
        return pieces.toArray(new Piece[0]);
    }
    */
    public Piece[] getAllPiecesFromBoard() {
        return Arrays.stream(board)
                .flatMap(Arrays::stream)
                .filter(Objects::nonNull)
                .toArray(Piece[]::new);
    }

    @Override
    public boolean equals(Object otherBoard) {
        if (this == otherBoard) {
            return true;
        }
        if (otherBoard == null || getClass() != otherBoard.getClass()) {
            return false;
        }
        Board board1 = (Board) otherBoard;
        return round == board1.round && Arrays.deepEquals(board, board1.board);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(round);
        result = 31 * result + Arrays.deepHashCode(board);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder boardString = new StringBuilder();
        boardString.append("  ");
        for (int i = 1; i <= SIZE; i++) {
            boardString.append("  ").append(i).append(" ");
        }
        boardString.append(System.lineSeparator());
        for (int row = 0; row < SIZE; row++) {
            boardString.append("  --------------------------------").append(System.lineSeparator());
            boardString.append(LETTER_NUMBER_NOTATION[row]).append(" ");
            for (int col = 0; col < SIZE; col++) {
                if (board[row][col] != null) {
                    boardString.append("| ").append(board[row][col]).append(" ");
                } else {
                    boardString.append("|   ");
                }
            }
            boardString.append("|");
            boardString.append(System.lineSeparator());
        }
        boardString.append("  --------------------------------");
        return boardString.toString();
    }

    @Override
    public Memento save() {
        Piece[][] boardCopy = new Piece[SIZE][SIZE];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == null) {
                    boardCopy[i][j] = null;
                } else {
                    boardCopy[i][j] = board[i][j];
                }
            }
        }
        return new Memento(boardCopy, round);
    }

    @Override
    public void restore(Memento save) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (save.board()[i][j] == null) {
                    board[i][j] = null;
                } else {
                    board[i][j] = save.board()[i][j];
                }
            }
        }
        round = save.round();
    }
}
